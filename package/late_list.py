"""
Late list

Этот модуль создаёт список из фамилий студентов,
которые вовремя не сдали работу.
"""

import datetime


def late_list(grades: dict, deadline_date: str) -> list[str]:
    """
    Определяет студентов, сдавших работу позже срока.

    Аргументы:
    :param grades: словарь (фамилия: дата сдачи)
    :param deadline_date: дата дедлайна работы

    Возвращаемое значение:
    :return: список студентов
    """
    dd_day, dd_month, dd_year = [int(number) for number in
                                 deadline_date.split(".")]
    dd_date = datetime.date(dd_year, dd_month, dd_day)

    students = []

    for student, date in grades.items():

        date_day, date_month, date_year = [int(number) for number
                                           in date.split(".")]
        date = datetime.date(date_year, date_month, date_day)

        delta = int((date - dd_date).days)

        if delta > 0:
            students.append(student)

    students.sort()

    return students
