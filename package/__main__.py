"""Точка входа"""

import re
import deadline_score as ds
import late_list as ll

if __name__ == "__main__":

    message = "Введите номер команды\n" \
              "1: Подсчёт количества баллов,\n" \
              "2: Определение количества должников,\n" \
              "> "

    chosen_command = input(message).strip()

    if chosen_command == "1":
        data = input("Введите две даты (дату сдачи и дату дедлайна) "
                     "через пробел\n"
                     "> ")
        dates = data.split()
        if len(dates) == 2:

            if re.match(r"\d\d[.]\d\d[.]\d{4}", dates[0]) and \
                    re.match(r"\d\d[.]\d\d[.]\d{4}", dates[1]):
                print("Баллы:", ds.deadline_score(dates[0], dates[1]))
            else:
                raise ValueError("Неверный ввод.")

        else:
            print("Неверный ввод.")

    elif chosen_command == "2":
        peoples = input("Введите пары (студент дата сдачи) через пробел\n"
                        "Пример: Иванов 03.09.2020 Петров 01.09.2020\n"
                        "> ")

        peoples = peoples.split()
        surnames = []

        if len(peoples) % 2 != 0 or len(peoples) < 2:
            print("Неверный ввод.")
        else:
            # Создаём список фамилий и дат сдачи
            for number in range(len(peoples) // 2):
                surnames.append(peoples[2 * number])
            pass_dates = list(set(peoples) - set(surnames))

            # Проверяем введённые данные
            checking_1 = [surname for surname in surnames if
                          re.match(r"[а-яА-Я]+", surname)]
            checking_2 = [date for date in pass_dates if
                          re.match(r"\d\d.\d\d.\d{4}", date)]

            if checking_1 == surnames and checking_2 == pass_dates:
                dictionary = {surname: date for surname, date in
                              zip(surnames, pass_dates)}

                deadline_date = input("Введите дату дедлайна\n"
                                      "Пример: 02.09.2020\n"
                                      "> ").strip()

                if re.match(r"\d\d[.]\d\d[.]\d{4}", deadline_date):
                    print("Список должников:", ll.late_list(dictionary,
                                                            deadline_date))
                else:
                    raise ValueError("Неверный ввод.")
            else:
                print("Неверный ввод.")

    else:
        print("Введён неверный номер команды.")
