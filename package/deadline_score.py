"""
Deadline score

Этот модуль позволяет определять количество
баллов за сданную работу.
"""

import datetime


def deadline_score(pass_date: str, deadline_date: str) -> int:
    """
    Определяет количество баллов за сданную работу.

    Аргументы:
    :param pass_date: дата сдачи работы
    :param deadline_date: дата дедлайна

    Возвращаемое значение:
    :return: mark
    """
    ps_day, ps_month, ps_year = [int(number) for number
                                 in pass_date.split(".")]
    dd_day, dd_month, dd_year = [int(number) for number
                                 in deadline_date.split(".")]

    ps_date = datetime.date(ps_year, ps_month, ps_day)
    dd_date = datetime.date(dd_year, dd_month, dd_day)

    delta = int((ps_date - dd_date).days)

    if delta <= 0:
        return 5

    mark = 0

    for factor in range(1, 6):
        if delta <= 7 * factor:
            mark = 5 - factor
            return mark

    return mark