"""Системный файл с фикстурой"""

import pytest

count_1, count_2 = 0, 0  # Номер теста


@pytest.fixture(scope="function", autouse=True)
def title(request):
    global count_1, count_2
    if "ds" in request.function.__name__:
        count_1 += 1
        print(f"{count_1} тест функции deadline_score")
    elif "ll" in request.function.__name__:
        count_2 += 1
        print(f"{count_2} тест функции late_list")
