"""Тесты"""

import pytest
from .. import deadline_score as ds
from .. import late_list as ll


@pytest.mark.parametrize("pass_date, deadline_date, scores", [("19.11.2021", "20.11.2021", 5),
                                                              ("10.10.2021", "04.10.2021", 4),
                                                              ("03.06.2021", "20.05.2021", 3),
                                                              ("22.4.2021", "2.4.2021", 2),
                                                              ("28.3.2021", "1.3.2021", 1),
                                                              ("05.11.2021", "05.10.2021", 0)])
# Проверяем работу функции deadline_score при верном вводе.
def test_ds_normal_data(pass_date, deadline_date, scores):
    assert ds.deadline_score(pass_date, deadline_date) == scores


@pytest.mark.parametrize("expected_exception, pass_date, deadline_date",
                         [(ValueError, "19.11.2021", "20/11/2021"),
                          (ValueError, "10.1", "5.1"),
                          (ValueError, "-10.01.2021", "-05.01.2021"),
                          (ValueError, "", ""),
                          (ValueError, "510.10.20201", "300.10.20201")])
# Проверяем работу функции deadline_score при неверном вводе.
def test_ds_wrong_data(pass_date, deadline_date, expected_exception):
    with pytest.raises(expected_exception):
        assert ds.deadline_score(pass_date, deadline_date)


@pytest.mark.parametrize("grades, deadline_date, roster",
                         [({"Иванов": "03.09.2021", "Петров": "01.09.2021"},
                           "02.09.2021", ["Иванов"]),
                          ({"Терентьев": "28.10.2021", "Ильин": "29.10.2021"},
                           "27.10.2021", ["Ильин", "Терентьев"])])
# Проверяем работу функции late_list при верном вводе
# информации о двух студентах.
def test_ll_two_students(grades, deadline_date, roster):
    assert ll.late_list(grades, deadline_date)


@pytest.mark.parametrize("grades, deadline_date, roster",
                         [({"Новиков": "18.09.2021", "Седов": "16.09.2021",
                            "Попов": "17.09.2021"}, "17.09.2021", ["Седов"]),
                          ({"Лебедев": "06.05.2021", "Васильев": "05.05.2021",
                            "Федоров": "04.05.2021"}, "03.05.2021", ["Васильев",
                                                                     "Лебедев",
                                                                     "Федоров"])])
# Проверяем работу функции late_list при верном вводе
# информации о трёх студентах.
def test_ll_three_students(grades, deadline_date, roster):
    assert ll.late_list(grades, deadline_date)


@pytest.mark.parametrize("expected_exception, grades, deadline_date",
                         [(ValueError, {"Николаев": "13.02.2021",
                                        "Морозов": "сегодня"},
                           "12.02.2021"),
                          (ValueError, {"Петров": "26.03.2021",
                                        "Егоров": ""},
                           "27.03.2021")])
# Проверяем работу функции late_list при неверном вводе
def test_ll_wrong_data(grades, deadline_date, expected_exception):
    with pytest.raises(expected_exception):
        assert ll.late_list(grades, deadline_date)
