# Пакет для Python
![Пакет для Python](https://python.robasworld.com/wp-content/uploads/2017/11/Python-Package.png)

Этот пакет предоставляет возможность определять
количество баллов за сданную работу. Также он позволяет
создавать список студентов, которые не сдали работу вовремя.

## Модули, входящие в состав пакета:
* deadline_score - Определение количества баллов за сданную работу,
исходя из даты дедлайна и фактической сдачи:
```python
"""
Deadline score

Этот модуль позволяет определять количество
баллов за сданную работу.
"""

import datetime


def deadline_score(pass_date: str, deadline_date: str) -> int:
    """
    Определяет количество баллов за сданную работу.

    Аргументы:
    :param pass_date: дата сдачи работы
    :param deadline_date: дата дедлайна

    Возвращаемое значение:
    :return: mark
    """
    ps_day, ps_month, ps_year = [int(number) for number
                                 in pass_date.split(".")]
    dd_day, dd_month, dd_year = [int(number) for number
                                 in deadline_date.split(".")]
    ...
```
* late_list - Создание списка из фамилий тех студентов, 
которые не сдали работу вовремя:
```python
"""
Late list

Этот модуль создаёт список из фамилий студентов,
которые вовремя не сдали работу.
"""

import datetime


def late_list(grades: dict, deadline_date: str) -> list[str]:
    """
    Определяет студентов, сдавших работу позже срока.

    Аргументы:
    :param grades: словарь (фамилия: дата сдачи)
    :param deadline_date: дата дедлайна работы

    Возвращаемое значение:
    :return: список студентов
    """
    dd_day, dd_month, dd_year = [int(number) for number in
                                 deadline_date.split(".")]
    dd_date = datetime.date(dd_year, dd_month, dd_day)
    ...
```
* \_\_main__ - Точка входа в программу, проверка ввода и вызов 
выбранной функции.
В неё импортируются другие модули:
```python
"""Точка входа"""

import re
import deadline_score as ds
import late_list as ll

if __name__ == "__main__":
    message = "Введите номер команды\n" \
              "1: Подсчёт количества баллов,\n" \
              "2: Определение количества должников,\n" \
              "> "
    
    chosen_command = input(message).strip()

    if chosen_command == "1":
        ...
    elif chosen_command == "2":
        ...
    ...
```
* \_\_init__ - Этот файл используется для 
инициализации пакета и перечисления всех модулей:
```python
"""
Пакет для Python.

Этот пакет предоставляет возможность определять
количество баллов за сданную работу. Также он позволяет
создавать список студентов, которые не сдали работу вовремя.

Этот пакет включает в себя следующие модули:

__main__ -- Точка входа в программу, проверка ввода и вызов выбранной функции.

deadline_score -- Определение количества баллов за сданную работу,
                  исходя из даты дедлайна и фактической сдачи.

late_list -- Создание списка из фамилий тех студентов,
                       которые не сдали работу вовремя.
"""

__version__ = '1.0.0'
__all__ = ['deadline_score', 'late_list']

__author__ = 'Nikita Terentev nitro-2003@mail.ru>'
```
## Тесты
![PyTest](https://unipython.com/wp-content/uploads/2020/04/pytest-framework-min.png)

Автоматические тесты для функций реализованы с помощью 
пакета [PyTest](https://docs.pytest.org/en/6.2.x/). Пример тестов:
```python
"""Тесты"""

import pytest
from .. import deadline_score as ds
from .. import late_list as ll


@pytest.mark.parametrize("pass_date, deadline_date, scores", [("19.11.2021", "20.11.2021", 5),
                                                              ("10.10.2021", "04.10.2021", 4),
                                                              ("03.06.2021", "20.05.2021", 3),
                                                              ("22.4.2021", "2.4.2021", 2),
                                                              ("28.3.2021", "1.3.2021", 1),
                                                              ("05.11.2021", "05.10.2021", 0)])
# Проверяем работу функции deadline_score при верном вводе.
def test_ds_normal_data(pass_date, deadline_date, scores):
    assert ds.deadline_score(pass_date, deadline_date) == scores
...
```
### Клонирование
Для того, чтобы склонировать данный репозиторий к себе на компьютер,
воспользуйтесь следующей коммандой:
```commandline
$ git clone https://gitlab.com/Nitrolacs/package.git
```
### Лицензия
[MIT License](https://mit-license.org) 
___
#### Полезные ссылки
https://git-scm.com
https://www.atlassian.com/ru/git/tutorials/comparing-workflows/gitflow-workflow
